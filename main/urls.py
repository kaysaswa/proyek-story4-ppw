from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('educations/', views.educations, name='educations'),
    path('experiences/', views.experiences, name='experiences'),
    path('project/', views.project, name='project'),
    path('project/story1', views.projectstory1, name='projectstory1'),
    path('contact/', views.contact, name='contact'),
]
