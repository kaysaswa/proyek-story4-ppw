from django.shortcuts import render


def home(request):
    page = 'HOME'
    response = {'title' : page}
    return render(request,'main/HOME1.html',response)

def about(request):
    page = 'ABOUT ME'
    response = {'title' : page}
    return render(request,'main/ABOUT ME1.html',response)

def educations(request):
    page = 'EDUCATIONS'
    response = {'title' : page}
    return render(request,'main/EDUCATIONS1.html',response)

def experiences(request):
    page = 'EXPERIENCES'
    response = {'title' : page}
    return render(request,'main/EXPERIENCES1.html',response)

def project(request):
    page = 'PROJECT'
    response = {'title' : page}
    return render(request,'main/PROJECT1.html',response)

def projectstory1(request):
    page = 'STORY1'
    response = {'title' : page}
    return render(request,'main/Biodata.html',response)

def contact(request):
    page = 'CONTACT'
    response = {'title' : page}
    return render(request,'main/CONTACT1.html',response)